import {
    GET_PHOTOS_FAIL_ACTION,
    GET_PHOTOS_SUCCESS_ACTION,
    IS_LOADING_ACTION,
    RESET_PAGE_ACTION,
    SELECT_PHOTO_ACTION
} from "./actionTypes"
import config from "../../config/config"

export const isLoadingAction = () => {
    return {
        type: IS_LOADING_ACTION
    }
}
export const getPhotosStartAction = () => {
    if (__DEV__) console.log("getPhotosStartAction")
    return async (dispatch, getState) => {
        if (getState().photo.isLoading) return
        dispatch(isLoadingAction())
        try {
            console.log("State in action: ", getState())
            const page = getState().photo.page
            const clientId = getState().photo.clientId
            const newPage = page + 1
            console.log("page:", newPage)
            const url = `${config.getPhotosUrl}?client_id=${clientId}&page=${newPage.toString()}&per_page=${config.photos_per_page}`
            //const resp = await fetch("http://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0")
            const resp = await fetch(url)
            console.log("Response from fetch: ", resp)
            let respJson = await resp.json()
            console.log("Json from fetch: ", respJson)
            //make response not that heavy
            respJson = respJson.map(e => {
                return {
                    id: e.id,
                    urls: {full: e.urls.full, thumb: e.urls.thumb},
                    user: {name: e.user.name},
                    description: e.description,
                    alt_description: e.alt_description
                }
            })
            console.log(">>> from fetch: ", respJson)
            if (resp.status === 200 && respJson.length > 0) {
                dispatch(getPhotosSuccessAction(respJson, newPage))
            }

        } catch (err) {
            console.log(err);
            dispatch(getPhotosFailAction(err))
        }
    }
}

export const getPhotosSuccessAction = (photos, page) => {
    return {
        type: GET_PHOTOS_SUCCESS_ACTION,
        payload: photos,
        page
    }
}
export const getPhotosFailAction = (err) => {
    return {
        type: GET_PHOTOS_FAIL_ACTION,
        payload: err
    }
}
export const selectPhotoAction = (id) => {
    return {
        type: SELECT_PHOTO_ACTION,
        payload: id
    }
}

export const pullToRefreshAction = () => (dispatch) => {
    dispatch(resetPageAction())
    dispatch(getPhotosStartAction())
}

export const resetPageAction = () => {
    return {
        type: RESET_PAGE_ACTION
    }
}
import {
    GET_PHOTOS_FAIL_ACTION,
    GET_PHOTOS_SUCCESS_ACTION,
    IS_LOADING_ACTION,
    RESET_PAGE_ACTION,
    SELECT_PHOTO_ACTION
} from "../actions/actionTypes"
import config from "../../config/config"

const INIT_STATE = {
    list: [],
    page: 0,
    selected: null,
    isLoading: false,
    error: null,
    clientId: config.clientId,
    isRefreshing: false

}
const photoReducer = (state = INIT_STATE, action) => {
    console.log("Photo reducer action: ", action)
    switch (action.type) {
        case GET_PHOTOS_SUCCESS_ACTION:
            let list = state.list.concat(action.payload)
            return {...state, list: list, isLoading: false, page: action.page, error: null, isRefreshing: false}

        case SELECT_PHOTO_ACTION:
            return {...state, selected: state.list.filter(i => i.id === action.payload)[0], error: null}
        case IS_LOADING_ACTION:
            return {...state, isLoading: true}
        case GET_PHOTOS_FAIL_ACTION:
            return {...state, isLoading: false, error: action.payload, isRefreshing: false}
        case RESET_PAGE_ACTION:
            return {...state, isRefreshing: true, page: 0, list: [], isLoading: false, error: null}
        default:
            return state
    }
}
export default photoReducer
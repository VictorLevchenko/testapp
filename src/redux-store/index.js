import reducers from "./reducers"
import {applyMiddleware, compose, createStore} from "redux"
import thunk from "redux-thunk"

let composeEnhancers = compose

if (__DEV__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
}

export default createStore(reducers, composeEnhancers(
    applyMiddleware(thunk))
)

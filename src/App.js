/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Provider} from 'react-redux'
import RootNavigator from "./navigation"
import store from './redux-store'
import {Root} from "native-base"

console.disableYellowBox = true

export default class App extends Component<Props> {
    render() {
        return (
            <Root>
                <Provider store={store}>

                    <RootNavigator/>

                </Provider>
            </Root>

        );
    }
}


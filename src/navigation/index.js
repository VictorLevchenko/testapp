import {createAppContainer, createStackNavigator} from "react-navigation"
import PhotoGalleryScreen from "../screens/PhotoGalleryScreen"
import PhotoDetailsScreen from "../screens/PhotoDetailsScreen"
import {Platform} from "react-native"

const RootNavigator = createAppContainer(
    createStackNavigator(
        {
            PhotoGallery: {
                screen: PhotoGalleryScreen,
                navigationOptions: {
                    title: "Gallery",
                    headerTitleStyle: Platform.select({
                        android: {textAlign: "center", flex: 1}
                    })

                }
            },
            PhotoDetails: {screen: PhotoDetailsScreen}
        },
        {
            initialRouteName: "PhotoGallery"
        }
    )
)
export default RootNavigator
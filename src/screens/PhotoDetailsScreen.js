import React, {Component} from 'react'
import {ActivityIndicator, Dimensions, StyleSheet, View} from 'react-native'
import {Image} from "react-native-elements"
import {connect} from "react-redux"

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

class PhotoDetailsScreen extends Component {

    render() {
        console.log("render PhotoDetails with props: ", this.props)
        return (
            <View style={styles.container}>

                <Image
                    source={{uri: this.props.photo.urls.full}}
                    style={{width: "100%", height: "100%"}}
                    PlaceholderContent={<ActivityIndicator/>}
                    resizeMode="contain"
                    resizeMethod="resize"
                />

            </View>
        )
    }
}

const mapStateToProps = state => {

    return {
        photo: state.photo.selected
    }
}
export default connect(mapStateToProps, {})(PhotoDetailsScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})


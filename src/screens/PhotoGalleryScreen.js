import React, {Component} from 'react'
import {ActivityIndicator, FlatList, RefreshControl, StyleSheet, View} from 'react-native'
import {connect} from "react-redux"
import {getPhotosStartAction, pullToRefreshAction, selectPhotoAction} from "../redux-store/actions/photoActions"
import {ListItem} from "react-native-elements"
import config from "../config/config"
import {Toast} from "native-base"

class PhotoGalleryScreen extends Component {

    componentDidMount() {
        this.props.getPhotosStartAction()
    }

    componentWillReceiveProps(nextProps) {
        const {error} = nextProps
        if (error) {
            Toast.show({
                text: error.message,
                duration: 2000,
                buttonText: "Ok"
            })
        }
    }

    onPressAvatar = (id) => {
        this.props.selectPhotoAction(id)
        this.props.navigation.navigate("PhotoDetails")
    }
    renderPhotoInList = ({item}) => {
        //console.log("render item:", item.id)
        const title = item.alt_description || item.description
        return (
            <ListItem
                leftAvatar={{
                    source: {uri: item.urls.thumb}, size: "xlarge",
                    rounded: false, onPress: () => this.onPressAvatar(item.id)
                }}
                title={title ? title : "Unnamed"}
                subtitle={item.user.name}
                titleStyle={styles.title}
            />)
    }

    loadMorePhotos = () => this.props.getPhotosStartAction();

    onPullToRefresh = () => {
        this.props.pullToRefreshAction();
    }

    render() {
        //console.log("render galery with props: ", this.props)
        console.log("render gallery")
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.photos}
                    keyExtractor={item => item.id}
                    renderItem={this.renderPhotoInList}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={parseInt(config.photos_per_page / 2)}
                    onEndReached={this.loadMorePhotos}
                    maxToRenderPerBatch={config.photos_per_page / 2}
                    removeClippedSubviews
                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.isRefreshing}
                            onRefresh={this.onPullToRefresh}
                        />
                    }
                />
            </View>
        )
    }
}

const mapStateToProps = state => {
    const {list, error, isRefreshing} = state.photo
    return {photos: list, error, isRefreshing}
}
export default connect(mapStateToProps, {
    getPhotosStartAction,
    selectPhotoAction,
    pullToRefreshAction
})(PhotoGalleryScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    title: {
        fontWeight: "bold"
    }
})

